#!/usr/bin/env python

__author__ = "Patiga"
__license__ = "AGPL-3.0-only"
__version__ = "0.1.0"
__email__ = "dev@patiga.eu"

""" Generate a Teeworlds/DDNet menu background from an image or video
Supports images, gifs and videos.
The produced map is saved as "Background.map" in the current directory.
At max 64 different frames can be embedded, which limits gifs and videos.
The script deduplicates images and minimizies the amount of quads and envelopes.
"""

import twmap
from PIL import Image, UnidentifiedImageError
import numpy as np
import os

def background_map():
    map = twmap.Map.empty("DDNet06")
    map.info.author = "Generated"
    map.info.license = "Inherits the media's license"
    map.info.version = __version__
    map.info.credits = "Patiga's twmap-py script 'menu-background'"

    background_group = map.groups.new()
    background_group.name = "Background"
    background_group.parallax_x = 0
    background_group.parallax_y = 0

    physics_group = map.groups.new_physics()
    # Add smallest game layer possible
    physics_group.layers.new_game(2, 2)
    return (map, background_group)

def image_to_quadslayer(map, group, img, quadslayer_name):
    aspect_ratio = img.size[0] / img.size[1]
    (width, height) = twmap.camera_dimensions(aspect_ratio)
    layer = group.layers.new_quads()
    layer.name = quadslayer_name
    quad = layer.quads.new(0, 0, width, height)
    layer.image = len(map.images)

    img_name = os.path.basename(img.filename)
    img_data = np.array(img)
    map.images.new_from_data(img_name, img_data)
    return quad

def image_to_map(img):
    map, background_group = background_map()
    rgba_img = img.convert("RGBA")
    rgba_img.filename = img.filename
    image_to_quadslayer(map, background_group, rgba_img, f"BG Image")
    return map

class QuadFrame:
    def __init__(self, img, offset, duration, frame):
        self.img_data = np.array(img)
        self.frames = [(offset, duration)]
        self.first_frame = frame

    def try_extend(self, img_data, offset, duration):
        if np.array_equal(self.img_data, img_data):
            self.frames.append((offset - self.frames[-1][0], duration))
            return True
        else:
            return False

class FrameStorage:
    def __init__(self, video_name):
        self.video_name = video_name
        self.quad_frames = []
        self.current_offset = 0
        self.current_frame = 0

    def next_frame(self, image, duration):
        image = image.convert("RGBA")
        self.current_frame += 1
        img_data = np.array(image)
        for quad_frame in self.quad_frames:
            if quad_frame.try_extend(img_data, self.current_offset, duration):
                self.current_offset += duration
                return
        if len(self.quad_frames) == 64:
            print(f"The video has more than 64 frames with different images, the video will be cut off after the {self.current_frame - 1}th frame")
            return False
        self.quad_frames.append(QuadFrame(image, self.current_offset, duration, self.current_frame))
        self.current_offset += duration
        return True

    def build(self):
        map, background_group = background_map()
        quads = []
        for quad_frame in self.quad_frames:
            img = Image.fromarray(quad_frame.img_data)
            img.filename = f"{quad_frame.first_frame}_{os.path.basename(self.video_name)}"
            layer_name = f"Frame {quad_frame.first_frame}"
            if len(quad_frame.frames) > 1:
                layer_name += " +"
            quad = image_to_quadslayer(map, background_group, img, layer_name)
            quads.append(quad)
        envs = dict()
        for quad_frame, quad in zip(self.quad_frames, quads):
            copied_frames = [f for f in quad_frame.frames]
            copied_frames[0] = (0, copied_frames[0][1])
            norm_frames = tuple(copied_frames)
            offset = 0
            if norm_frames not in envs:
                env_index = len(map.envelopes)
                env = map.envelopes.new("Color")
                for f in norm_frames:
                    offset += f[0]
                    p1 = env.points.new(offset)
                    p1.curve = "Step"
                    p2 = env.points.new(offset + f[1])
                    p2.curve = "Step"
                    p2.a = 0
                env.points.new(self.current_offset)
                envs[norm_frames] = env_index
            quad.color_env = envs[norm_frames]
            quad.color_env_offset = self.current_offset - quad_frame.frames[0][0]
        return map
        
def gif_to_map(gif):
    frames = gif.n_frames
    builder = FrameStorage(gif.filename)
    gif.seek(0)
    while True:
        try:
            duration = gif.info['duration']
            processed = builder.next_frame(gif, duration)
            if not processed:
                # Image capacity of map is reached
                return builder.build()
            gif.seek(img.tell() + 1)
        except EOFError:
            # End of video fil# End of video file
            return builder.build()


def video_path_to_map(path):
    import cv2
    video = cv2.VideoCapture(path)
    assert(video.isOpened())

    frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = video.get(cv2.CAP_PROP_FPS)
    full_duration = int(1000 / fps * frames)
    frame_time = full_duration / frames

    builder = FrameStorage(os.path.basename(path))
    not_ended, frame = video.read()
    i = 0
    while not_ended:
        converted = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(converted)

        current_time = int(frame_time * i)
        next_time = int(frame_time * (i + 1))
        current_frame_duration = next_time - current_time
        processed = builder.next_frame(img, current_frame_duration)
        if not processed:
            break
        not_ended, frame = video.read()
        i += 1


    video.release()
    return builder.build()

if __name__ == "__main__":
    import sys

    args = sys.argv
    if len(args) != 2:
        print("Wrong number of arguments, expected one path to a media file")
        exit(1)
    path = args[1]
    try:
        img = Image.open(path)
        if getattr(img, "is_animated", False):
            map = gif_to_map(img)
        else:
            map = image_to_map(img)
    except UnidentifiedImageError:
        map = video_path_to_map(path)
    map.save("Background.map")

