#!/usr/bin/env python3
# Precomputes textures and color-to-indices mappings for the script pixelart.py.
# Use pypy3 to execute this code to save precious time.
#
# Author: Patiga
# License: AGPL

# For quick testing, set this to False
ENABLE_TESTS = True
FIRST_FRAC = 127 / 256
SECOND_FRAC = 128 / 256
def mix2(a, b):
    return round(FIRST_FRAC * a + SECOND_FRAC * b)

def mix3(a, b, c):
    return mix2(c, mix2(a, b))

def test_val(A, B, C, v):
    for a in A:
        for b in B:
            for c in C:
                if mix3(a, b, c) == v:
                    return True
    return False

def missing_values(A, B, C):
    return [v for v in range(256) if not test_val(A, B, C, v)]

def missing_values_permutate(As, Bs, Cs):
    best = ([], [], [])
    minimum = list(range(256))
    for A in As:
        for B in Bs:
            for C in Cs:
                missing = missing_values(A, B, C)
                if len(missing) < len(minimum):
                    best = (A, B, C)
                    minimum = missing
    return (best, minimum)

A1 = [i * 4 + 0 for i in range(64)]
A2 = [i * 4 + 1 for i in range(64)]
A3 = [i * 4 + 2 for i in range(64)]
A4 = [i * 4 + 3 for i in range(64)]
B1 = [0, 255]
B2 = [1, 255]
B3 = [2, 255]
B4 = [3, 255]
As = [A1, A2, A3, A4]
Bs = [B1, B2, B3, B4]

def rotate(triple):
    a, b, c = triple
    return (c, a, b)

def rotate_n(triple, n):
    for _ in range(n):
        triple = rotate(triple)
    return triple

# This is the collection order for red (since its dedicated layer is the first one)
# We use the rotate function to get the order for green and blue
order = (As, Bs, Bs)
colors = ["red", "green", "blue"]

# ---Above is code for calculating the best value collections---
# ---Below is code for creating the mapping from pixel to texture indices

def find_single_mapping(A, B, C, v):
    for a in A:
        for b in B:
            for c in C:
                if mix3(a, b, c) == v:
                    return (a, b, c)
    return None

def find_mapping(A, B, C):
    return [find_single_mapping(A, B, C, v) for v in range(256)]

def fill_missing(mapping):
    for i in range(256):
        if mapping[i] is None:
            if i + 1 < 256 and mapping[i + 1] is not None:
                mapping[i] = mapping[i + 1]
            else:
                mapping[i] = mapping[i - 1]
            assert(mapping[i] is not None)

def primary(value):
    return value // 4
def secondary(value):
    return value // 128 * 128
def tertiary(value):
    return value // 128 * 64
layer_value_to_index = (primary, secondary, tertiary)

def map_triple(fns, vals):
    return (fns[0](vals[0]), fns[1](vals[1]), fns[2](vals[2]))
def map2_triple(fn, v1, v2):
    return (fn(v1[0], v2[0]), fn(v1[1], v2[1]), fn(v1[2], v2[2]))
def map3_triple(fns, v1, v2, v3):
    return (fns(v1[0], v2[0], v3[0]), fns(v1[1], v2[1], v3[1]), fns(v1[2], v2[2], v3[2]))

# i is the index of the color
def convert_to_index_mapping(mapping, i):
    fns = rotate_n(layer_value_to_index, i)
    return [map_triple(fns, vals) for vals in mapping]

index_mappings = ({}, {}, {})

def retrieve_indices(pixel):
    r1, r2, r3 = index_mappings[0][pixel[0]]
    g1, g2, g3 = index_mappings[1][pixel[1]]
    b1, b2, b3 = index_mappings[2][pixel[2]]
    return (r1 + g1 + b1, r2 + g2 + b2, r3 + g3 + b3)

if __name__ == "__main__":
    index_mappings = []
    color_value_collections = []
    missings = []
    for i in range(3):
        color = colors[i]
        # values: For this color, a triple where each element is the list of values we have for this color in that layer
        values, missing = missing_values_permutate(*rotate_n(order, i))
        color_value_collections.append(values)
        missings.append(set(missing))
        print(f"# {color} values not representable: {len(missing)} ({missing})")
        print(f"{color}s = {values}")
        mapping = find_mapping(*values)
        assert(sum(val is None for val in mapping) == len(missing))
        fill_missing(mapping)
        index_mapping = convert_to_index_mapping(mapping, i)
        index_mappings.append(index_mapping)
    print(f"index_mappings = {index_mappings}")

    # Create all textures
    textures = []
    for i in range(3):
        texture = []
        # We always iterate through the 64 different values first, then the 0/255 pairs
        V1 = color_value_collections[(i + 0) % 3][i]
        V2 = color_value_collections[(i + 1) % 3][i]
        V3 = color_value_collections[(i + 2) % 3][i]
        for v3 in V3:
            for v2 in V2:
                for v1 in V1:
                    pixel = rotate_n((v1, v2, v3), i)
                    texture.append(pixel + (255,))
        assert(len(texture) == 256)
        textures.append(texture)
    print(f"textures = {textures}")
    if ENABLE_TESTS:
        for a in range(256):
            for b in range(256):
                for c in range(256):
                    pixel = (a, b, c)
                    indices = retrieve_indices(pixel)
                    def index(collection, i):
                        return collection[i]
                    pixels = map2_triple(index, textures, indices)
                    mixed = map3_triple(mix3, *pixels)
                    for i in range(3):
                        original = pixel[i]
                        new = mixed[i]
                        if new != original:
                            if abs(original - new) > 2:
                                print(original, new)
                                assert("The mixing values do not pass the test" == None)
    print("Color channels off-by-two: {}")
