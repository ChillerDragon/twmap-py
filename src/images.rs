use twmap::convert::TryTo;
use twmap::{EmbeddedImage, ExternalImage, Image, Load, TwMap};

use crate::map_err;
use crate::sequence_wrapping::*;

use numpy::{PyArray, PyArray3};
use pyo3::{prelude::*, types::PyString};
use vek::Extent2;

use image::{ImageError, RgbaImage};

use std::sync::{Arc, Mutex};

#[pyclass(name = "Images")]
#[derive(Clone)]
pub struct PyImages(pub Arc<Mutex<SequenceIndex<(), ()>>>);

/// Image object which wraps both external and embedded images.
/// Images are returned as 3d numpy arrays, with the dimensions [height, width, 4].
/// If a image is external, None will be returned as the data.
/// Use `embed` on the image or the entire map struct to embed external images.
/// The last dimension consists of the u8 rgba values.
///
/// Attributes: name, data
#[pyclass(name = "Image")]
#[derive(Clone)]
pub struct PyImage(pub Arc<Mutex<Indexable<(), ()>>>);

py_types!(PyImage, PyImages, (), ());

fn rgba_image_from_py_array3(py_array3: &PyArray3<u8>) -> PyResult<RgbaImage> {
    let dims = py_array3.shape();
    if dims[2] != 4 {
        return Err(pyo3::exceptions::PyValueError::new_err(
            "The last dimension of the image array must be of size 4",
        ));
    }
    let (height, width) = (dims[0], dims[1]);
    let pixel_data: Vec<u8> = match py_array3.readonly().as_slice() {
        Ok(slice) => slice.to_owned(),
        Err(_) => {
            return Err(pyo3::exceptions::PyValueError::new_err(
                "The array is not contiguous",
            ))
        }
    };
    Ok(RgbaImage::from_raw(width.try_to(), height.try_to(), pixel_data).unwrap())
}

impl MapNavigating for Image {
    type Entry = ();
    type Seq = ();
    type PyEntry = PyImage;
    type PySeq = PyImages;

    fn navigate_to_sequence<'a>(
        _: &SequenceIndex<(), ()>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Image>> {
        Ok(&mut map.images)
    }

    fn getattr(element: &PyImage, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let py_mage = element.depythonize().lock().unwrap();
        let mut map = py_mage.map.lock().unwrap();
        let image = Image::navigate_to_object(&py_mage, &mut map)?;
        Ok(Some(match attr {
            "name" => image.name().clone().into_py(py),
            "data" => {
                let embedded_image = match image {
                    Image::External(_) => return Ok(Some(<Option<()>>::None.into_py(py))),
                    Image::Embedded(image) => image,
                };
                embedded_image.image.load().map_err(map_err)?;
                let image = embedded_image.image.unwrap_ref();
                let (height, width) = (image.height() as usize, image.width() as usize);
                let image_data = embedded_image.image.unwrap_ref().as_raw();
                let py_image_array = PyArray::from_slice(py, image_data);
                py_image_array.reshape((height, width, 4))?.into()
            }
            _ => return Ok(None),
        }))
    }

    fn setattr(element: &PyImage, attr: &str, value: PyObject, py: Python) -> PyResult<Option<()>> {
        let py_image = element.depythonize().lock().unwrap();
        let mut map = py_image.map.lock().unwrap();
        let image = Image::navigate_to_object(&py_image, &mut map)?;
        match attr {
            "name" => *image.name_mut() = value.extract(py)?,
            "data" => match value.is_none(py) {
                true => {
                    *image = Image::External(ExternalImage {
                        name: image.name().to_owned(),
                        size: image.size(),
                    })
                }
                false => {
                    let py_array3: &PyArray3<u8> = value.extract(py)?;
                    *image = Image::Embedded(EmbeddedImage {
                        name: image.name().to_owned(),
                        image: rgba_image_from_py_array3(py_array3)?.into(),
                    })
                }
            },
            _ => return Ok(None),
        }
        Ok(Some(()))
    }
}

do_entry_impls!(
    Image,
    PyImage,
    PyImages,
    fn width(&self) -> PyResult<u32> {
        Image::access(self, |image| Ok(image.size().w))
    }
    fn height(&self) -> PyResult<u32> {
        Image::access(self, |image| Ok(image.size().h))
    }
    fn is_external(&self) -> PyResult<bool> {
        Image::access(self, |image| {
            Ok(match image {
                Image::External(_) => true,
                Image::Embedded(_) => false,
            })
        })
    }
    fn is_embedded(&self) -> PyResult<bool> {
        Ok(!self.is_external()?)
    }
    /// Embeds an external images using the mapres in the provided directory
    fn embed(&self, path: &str) -> PyResult<()> {
        Image::access(self, |image| match image {
            Image::External(ex) => {
                *image = Image::Embedded(ex.embed(path).map_err(map_err)?);
                Ok(())
            }
            Image::Embedded(_) => Err(pyo3::exceptions::PyValueError::new_err(
                "Embedded images can't be embedded",
            )),
        })
    }
    fn save(&self, path: &str) -> PyResult<()> {
        Image::access(self, |image| {
            if let Image::Embedded(emb) = image {
                emb.image.load().map_err(map_err)?;
                emb.image.unwrap_ref().save(path).map_err(map_err)
            } else {
                Err(pyo3::exceptions::PyValueError::new_err(
                    "This image is external, meaning it can't be saved",
                ))
            }
        })
    }
);

do_sequence_impls!(
    Image,
    PyImage,
    PyImages,
    /// Constructs an embedded image from a file.
    /// Tries to load the image specified by the file path, only png images are supported.
    fn new_from_file(&self, path: &str) -> PyResult<PyImage> {
        let new_image = match EmbeddedImage::from_file(path) {
            Err(img_error) => match img_error {
                ImageError::IoError(err) => {
                    return Err(pyo3::exceptions::PyIOError::new_err(err.to_string()))
                }
                _ => {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        img_error.to_string(),
                    ))
                }
            },
            Ok(emb) => Image::Embedded(emb),
        };
        append_wrapped(self, new_image)
    }
    /// Constructs an external image with the specified name.
    fn new_external(&self, name: &str) -> PyResult<PyImage> {
        let new_image = Image::External(ExternalImage {
            name: name.to_owned(),
            size: Extent2::broadcast(u32::MAX),
        });
        append_wrapped(self, new_image)
    }
    /// Constructs an embedded image with the specified name and data.
    fn new_from_data(&self, name: &str, py_array3: &PyArray3<u8>) -> PyResult<PyImage> {
        let new_image = Image::Embedded(EmbeddedImage {
            name: name.to_owned(),
            image: rgba_image_from_py_array3(py_array3)?.into(),
        });
        append_wrapped(self, new_image)
    }
);
