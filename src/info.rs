use twmap::TwMap;

use pyo3::prelude::*;
use pyo3::types::PyString;
use pyo3::PyNativeType;

use std::sync::{Arc, Mutex};

/// Map info object
///
/// Attributes:
///     author
///     version
///     credits
///     license
///     settings
///         Note that retrieving the settings will give you a python-owned list of strings.
///         This means only changing that list will not change the settings in the map, you need to put it back into the attribute
#[pyclass(name = "Info")]
#[derive(Clone)]
pub(crate) struct PyInfo {
    pub map: Arc<Mutex<TwMap>>,
}

#[pymethods]
impl PyInfo {
    fn __getattr__(&self, attr: &PyString) -> PyResult<PyObject> {
        let py = attr.py();
        let attr = attr.to_str()?;
        let info = &self.map.lock().unwrap().info;
        Ok(match attr {
            "author" => info.author.clone().into_py(py),
            "version" => info.version.clone().into_py(py),
            "credits" => info.credits.clone().into_py(py),
            "license" => info.license.clone().into_py(py),
            "settings" => info.settings.clone().into_py(py),
            _ => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(format!(
                    "This object has no attribute called '{}'",
                    attr
                )))
            }
        })
    }

    fn __setattr__(&mut self, attr: &PyString, value: PyObject) -> PyResult<()> {
        let py = attr.py();
        let attr = attr.to_str()?;
        let info = &mut self.map.lock().unwrap().info;
        match attr {
            "author" => info.author = value.extract(py)?,
            "version" => info.version = value.extract(py)?,
            "credits" => info.credits = value.extract(py)?,
            "license" => info.license = value.extract(py)?,
            "settings" => info.settings = value.extract(py)?,
            _ => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(format!(
                    "This object has no attribute called '{}'",
                    attr
                )))
            }
        }
        Ok(())
    }
}
