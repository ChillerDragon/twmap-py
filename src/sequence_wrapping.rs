use std::cmp::Ordering;
use twmap::convert::TryTo;
use twmap::TwMap;

use pyo3::prelude::*;
use pyo3::PyNativeType;

use pyo3::types::PyString;

use std::collections::HashMap;
use std::iter;
use std::sync::{Arc, Mutex, Weak};

pub struct Indexable<T, U> {
    id: u32,
    sequence: Arc<Mutex<SequenceIndex<T, U>>>,
    pub map: Arc<Mutex<TwMap>>,
    pub value: T,
}

pub struct SequenceIndex<T, U> {
    mapping: Vec<Weak<Mutex<Indexable<T, U>>>>,
    id_lookup: HashMap<u32, usize>,

    self_ref: Weak<Mutex<Self>>, // Self-reference

    map: Arc<Mutex<TwMap>>,
    pub predecessor: U,

    next_id: u32,
}

pub struct SequenceInitializer<T: Default, U>(Weak<Mutex<SequenceIndex<T, U>>>);

impl<T: Default, U> Default for SequenceInitializer<T, U> {
    fn default() -> Self {
        SequenceInitializer(Weak::new())
    }
}

impl<T: Default, U> SequenceInitializer<T, U> {
    pub fn get(
        &mut self,
        len: usize,
        map: Arc<Mutex<TwMap>>,
        predecessor: U,
    ) -> Arc<Mutex<SequenceIndex<T, U>>> {
        match self.0.upgrade() {
            None => {
                let seq = SequenceIndex::new(len, map, predecessor);
                self.0 = Arc::downgrade(&seq);
                seq
            }
            Some(seq) => seq,
        }
    }

    pub fn try_get(&self) -> Option<Arc<Mutex<SequenceIndex<T, U>>>> {
        self.0.upgrade()
    }
}

impl<T: Default, U> SequenceIndex<T, U> {
    pub fn retrieve_shared(&mut self, index: usize) -> Arc<Mutex<Indexable<T, U>>> {
        let weak = &mut self.mapping[index];
        match weak.upgrade() {
            Some(indexed) => indexed,
            None => {
                self.next_id += 1;
                let indexed = Arc::new(Mutex::new(Indexable {
                    id: self.next_id,
                    sequence: self.self_ref.upgrade().unwrap(),
                    map: self.map.clone(),
                    value: T::default(),
                }));
                *weak = Arc::downgrade(&indexed);
                self.id_lookup.insert(self.next_id, index);
                indexed
            }
        }
    }

    pub fn new(len: usize, map: Arc<Mutex<TwMap>>, predecessor: U) -> Arc<Mutex<Self>> {
        let arc = Arc::new(Mutex::new(SequenceIndex {
            mapping: iter::repeat(Weak::new()).take(len).collect(),
            id_lookup: HashMap::new(),
            self_ref: Weak::new(),
            map,
            predecessor,
            next_id: 0,
        }));
        arc.lock().unwrap().self_ref = Arc::downgrade(&arc);
        arc
    }

    pub fn lookup(&self, id: u32) -> Option<usize> {
        self.id_lookup.get(&id).copied()
    }

    pub fn invalidate(&mut self, len: usize) {
        self.mapping = iter::repeat(Weak::new()).take(len).collect();
        self.id_lookup = HashMap::new();
    }

    pub fn remap_id_lookup(&mut self, mapping: impl Fn(usize) -> usize) {
        self.id_lookup
            .values_mut()
            .for_each(|index| *index = mapping(*index))
    }

    pub fn delete_shift(&mut self, index: usize) {
        let entry = &self.mapping[index];
        if let Some(id) = entry.upgrade().map(|entry| entry.lock().unwrap().id) {
            self.id_lookup.remove(&id);
        } else if let Some(id) = self
            .id_lookup
            .iter()
            .find(|(_, &i)| i == index)
            .map(|(id, _)| *id)
        {
            self.id_lookup.remove(&id);
        }
        self.mapping.remove(index);
        self.remap_id_lookup(|old_index| match old_index.cmp(&index) {
            Ordering::Less => old_index,
            Ordering::Equal => panic!("Remove failed"),
            Ordering::Greater => old_index - 1,
        })
    }

    pub fn push(&mut self) {
        self.mapping.push(Weak::new())
    }

    pub fn insert_at(&mut self, index: usize) {
        self.mapping.insert(index, Weak::new());
        self.remap_id_lookup(|old_index| {
            if old_index >= index {
                old_index + 1
            } else {
                old_index
            }
        })
    }
}

pub trait PySequence<T: Default, U>: Sized {
    fn _pythonize(seq: Arc<Mutex<SequenceIndex<T, U>>>) -> Self;

    fn depythonize(&self) -> &Arc<Mutex<SequenceIndex<T, U>>>;
}

pub trait PyEntry<T, U> {
    fn pythonize(entry: Arc<Mutex<Indexable<T, U>>>) -> Self;

    fn depythonize(&self) -> &Arc<Mutex<Indexable<T, U>>>;
}

pub trait MapNavigating: Clone {
    type Entry: Default;
    type Seq;

    type PyEntry: PyEntry<Self::Entry, Self::Seq>;
    type PySeq: PySequence<Self::Entry, Self::Seq>;

    fn navigate_to_sequence<'a>(
        sequence: &SequenceIndex<Self::Entry, Self::Seq>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Self>>;

    fn navigate_to_object<'a>(
        element: &Indexable<Self::Entry, Self::Seq>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Self> {
        let sequence = element.sequence.lock().unwrap();
        let wrapped_vec = Self::navigate_to_sequence(&sequence, map)?;
        let index = sequence
            .lookup(element.id)
            .ok_or_else(|| pyo3::exceptions::PyLookupError::new_err("This object was removed"))?;
        let wrapped = &mut wrapped_vec[index];
        Ok(wrapped)
    }

    fn getattr(element: &Self::PyEntry, attr: &str, py: Python) -> PyResult<Option<PyObject>>;

    fn setattr(
        element: &Self::PyEntry,
        attr: &str,
        value: PyObject,
        py: Python,
    ) -> PyResult<Option<()>>;

    /// Locks everything, don't use while anything is locked!
    fn access<T>(
        element: &Self::PyEntry,
        function: impl FnOnce(&mut Self) -> PyResult<T>,
    ) -> PyResult<T> {
        let element = element.depythonize().lock().unwrap();
        let mut map = element.map.lock().unwrap();
        let wrapped = Self::navigate_to_object(&element, &mut map)?;
        function(wrapped)
    }

    fn access_sequence<T: Sized>(
        sequence: &Self::PySeq,
        function: impl FnOnce(&mut Vec<Self>) -> PyResult<T>,
    ) -> PyResult<T> {
        let sequence = sequence.depythonize().lock().unwrap();
        let mut map = sequence.map.lock().unwrap();
        let object = Self::navigate_to_sequence(&sequence, &mut map)?;
        function(object)
    }
}

/// Note that this does not validate that the sequence is still accessible
pub fn __len__<T: MapNavigating>(sequence: &T::PySeq) -> PyResult<usize> {
    Ok(T::PySeq::depythonize(sequence)
        .lock()
        .unwrap()
        .mapping
        .len())
}

pub fn __getitem__<T: MapNavigating>(sequence: &T::PySeq, index: isize) -> PyResult<T::PyEntry> {
    if index < 0 {
        return Err(pyo3::exceptions::PyIndexError::new_err(format!(
            "Index {} is out of bounds, the list has {} elements",
            index,
            __len__::<T>(sequence)?
        )));
    }
    let index: usize = index.try_to();
    if index >= __len__::<T>(sequence)? {
        return Err(pyo3::exceptions::PyIndexError::new_err(format!(
            "Index {} is out of bounds, the list has {} elements",
            index,
            __len__::<T>(sequence)?
        )));
    }
    let mut sequence = T::PySeq::depythonize(sequence).lock().unwrap();
    Ok(T::PyEntry::pythonize(sequence.retrieve_shared(index)))
}

pub fn __setitem__<T: MapNavigating>(
    sequence: &T::PySeq,
    index: isize,
    item: &T::PyEntry,
) -> PyResult<()> {
    if index < 0 {
        return Err(pyo3::exceptions::PyIndexError::new_err(format!(
            "Index {} is out of bounds, the list has {} elements",
            index,
            __len__::<T>(sequence)?
        )));
    }
    let index: usize = index.try_to();
    if index >= __len__::<T>(sequence)? {
        return Err(pyo3::exceptions::PyIndexError::new_err(format!(
            "Index {} is out of bounds, the list has {} elements",
            index,
            __len__::<T>(sequence)?
        )));
    }

    let item = T::access(item, |e| Ok(e.clone()))?;
    T::access_sequence(sequence, |vec| {
        vec[index] = item;
        Ok(())
    })?;
    Ok(())
}

pub fn __delitem__<T: MapNavigating>(sequence: &T::PySeq, index: isize) -> PyResult<()> {
    if index < 0 {
        return Err(pyo3::exceptions::PyIndexError::new_err(format!(
            "Index {} is out of bounds, the list has {} elements",
            index,
            __len__::<T>(sequence)?
        )));
    }
    let index: usize = index.try_to();
    if index >= __len__::<T>(sequence)? {
        return Err(pyo3::exceptions::PyIndexError::new_err(format!(
            "Index {} is out of bounds, the list has {} elements",
            index,
            __len__::<T>(sequence)?
        )));
    }
    T::access_sequence(sequence, |vec| Ok(vec.remove(index)))?;
    let mut sequence = T::PySeq::depythonize(sequence).lock().unwrap();
    sequence.delete_shift(index);
    Ok(())
}

pub fn __contains__<T: MapNavigating>(sequence: &T::PySeq, item: &T::PyEntry) -> PyResult<bool> {
    // TODO: maybe check if the sequence actually still exists
    let arc_sequence = sequence.depythonize();
    let entry = item.depythonize().lock().unwrap();
    if !Arc::ptr_eq(arc_sequence, &entry.sequence) {
        return Ok(false); // Object is not from the same sequence object
    }

    let sequence = arc_sequence.lock().unwrap();
    Ok(sequence.id_lookup.contains_key(&entry.id))
}

pub fn append_wrapped<T: MapNavigating>(sequence: &T::PySeq, item: T) -> PyResult<T::PyEntry> {
    let index = T::access_sequence(sequence, |vec| {
        vec.push(item);
        Ok(vec.len() - 1)
    })?;
    T::PySeq::depythonize(sequence).lock().unwrap().push();
    __getitem__::<T>(sequence, index.try_to())
}

pub fn append_py<T: MapNavigating>(sequence: &T::PySeq, item: &T::PyEntry) -> PyResult<T::PyEntry> {
    let item = T::access(item, |e| Ok(e.clone()))?;
    append_wrapped(sequence, item)
}

pub fn insert_wrapped_at<T: MapNavigating>(
    sequence: &T::PySeq,
    index: usize,
    item: T,
) -> PyResult<T::PyEntry> {
    let len = T::access_sequence(sequence, |vec| Ok(vec.len()))?;
    if index > len {
        return Err(pyo3::exceptions::PyIndexError::new_err(
            "Index to insert at is too big",
        ));
    }
    T::PySeq::depythonize(sequence)
        .lock()
        .unwrap()
        .insert_at(index);
    T::access_sequence(sequence, |vec| {
        vec.insert(index, item);
        Ok(())
    })?;
    __getitem__::<T>(sequence, index.try_to())
}

pub fn insert_py<T: MapNavigating>(
    sequence: &T::PySeq,
    index: usize,
    item: &T::PyEntry,
) -> PyResult<T::PyEntry> {
    let item = T::access(item, |e| Ok(e.clone()))?;
    insert_wrapped_at(sequence, index, item)
}

pub fn __getattr__<T: MapNavigating>(element: &T::PyEntry, attr: &PyString) -> PyResult<PyObject> {
    let py = attr.py();
    let attr = attr.to_str()?;
    match T::getattr(element, attr, py) {
        Ok(None) => Err(pyo3::exceptions::PyAttributeError::new_err(format!(
            "This object has no attribute called '{}'",
            attr
        ))),
        attr_result => attr_result.map(|some| some.unwrap()),
    }
}

pub fn __setattr__<T: MapNavigating>(
    element: &T::PyEntry,
    attr: &PyString,
    value: PyObject,
) -> PyResult<()> {
    let py = attr.py();
    let attr = attr.to_str()?;
    match T::setattr(element, attr, value, py) {
        Ok(None) => Err(pyo3::exceptions::PyAttributeError::new_err(format!(
            "This object has no attribute called '{}'",
            attr
        ))),
        Err(err) => Err(err),
        Ok(Some(())) => Ok(()),
    }
}

macro_rules! py_types {
    ($entry_py_type: ty, $seq_py_type: ty, $entry_type: ty, $seq_type: ty) => {
        impl PyEntry<$entry_type, $seq_type> for $entry_py_type {
            fn pythonize(entry: Arc<Mutex<Indexable<$entry_type, $seq_type>>>) -> Self {
                Self(entry)
            }

            fn depythonize(&self) -> &Arc<Mutex<Indexable<$entry_type, $seq_type>>> {
                &self.0
            }
        }

        impl PySequence<$entry_type, $seq_type> for $seq_py_type {
            fn _pythonize(seq: Arc<Mutex<SequenceIndex<$entry_type, $seq_type>>>) -> Self {
                Self(seq)
            }

            fn depythonize(&self) -> &Arc<Mutex<SequenceIndex<$entry_type, $seq_type>>> {
                &self.0
            }
        }
    };
}

macro_rules! do_entry_impls {
    ($wrapped: ty, $entry_py_type: ty, $seq_py_type: ty, $($rest: tt) *) => {
        #[pymethods]
        impl $entry_py_type {
            pub fn __getattr__(&self, attr: &PyString) -> PyResult<PyObject> {
                __getattr__::<$wrapped>(self, attr)
            }

            pub fn __setattr__(&mut self, attr: &PyString, value: PyObject) -> PyResult<()> {
                __setattr__::<$wrapped>(self, attr, value)
            }

            $( $rest )*
        }
    };
}

macro_rules! do_sequence_impls {
    ($wrapped: ty, $entry_py_type: ty, $seq_py_type: ty, $($rest: tt) *) => {
        #[pymethods]
        impl $seq_py_type {
            fn __len__(&self) -> PyResult<usize> {
                __len__::<$wrapped>(self)
            }

            pub fn __getitem__(&self, index: isize) -> PyResult<$entry_py_type> {
                __getitem__::<$wrapped>(self, index)
            }

            fn __setitem__(&mut self, index: isize, item: $entry_py_type) -> PyResult<()> {
                __setitem__::<$wrapped>(self, index, &item)
            }

            fn __delitem__(&mut self, index: isize) -> PyResult<()> {
                __delitem__::<$wrapped>(self, index)
            }

            fn __contains__(&self, item: $entry_py_type) -> PyResult<bool> {
                __contains__::<$wrapped>(self, &item)
            }

            /// Append an object to this sequence
            fn append(&self, item: $entry_py_type) -> PyResult<$entry_py_type> {
                append_py::<$wrapped>(self, &item)
            }

            /// Insert an object at an specific index
            fn insert(&self, index: usize, item: $entry_py_type) -> PyResult<$entry_py_type> {
                insert_py::<$wrapped>(self, index, &item)
            }

            $( $rest )*
        }
    }
}
