use twmap::{AnyLayer, Layer, SoundArea, SoundSource, SoundsLayer, TwMap};

use crate::layers::PyLayer;
use crate::sequence_wrapping::*;
use crate::{depy2, fixed_py, point_to_tup, py_fixed};

use pyo3::{prelude::*, types::PyString};
use vek::{Disk, Rect};

use std::sync::{Arc, Mutex};

#[pyclass(name = "SoundSources")]
#[derive(Clone)]
/// Sound source object of the Sounds layer
///
/// Attributes:
///     position - middle point of the sound source
///     looping
///     panning
///     delay
///     falloff
///     shape - just radius if it is a circular sound source, (width, height) if it is rectangular
///     position_env - index of the position envelope
///     position_env_offset - time offset of the position envelope, in ms
///     sound_env - index of the sound envelope
///     sound_env_offset - time offset of the sound envelope, in ms
pub struct PySources(pub Arc<Mutex<SequenceIndex<(), PyLayer>>>);

#[pyclass(name = "SoundSource")]
#[derive(Clone)]
/// Sound source object of the Sounds layer.
///
/// Attributes: position, looping, panning, delay, falloff, shape, position_env, position_env_offset, sound_env, sound_env_offset
pub struct PySource(pub Arc<Mutex<Indexable<(), PyLayer>>>);

py_types!(PySource, PySources, (), PyLayer);

impl MapNavigating for SoundSource {
    type Entry = ();
    type Seq = PyLayer;
    type PyEntry = PySource;
    type PySeq = PySources;

    fn navigate_to_sequence<'a>(
        sequence: &SequenceIndex<(), PyLayer>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<SoundSource>> {
        let py_layer = sequence.predecessor.depythonize().lock().unwrap();
        let layer = Layer::navigate_to_object(&py_layer, map)?;
        Ok(&mut SoundsLayer::get_mut(layer).unwrap().sources)
    }

    fn getattr(element: &PySource, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let py_layer = element.depythonize().lock().unwrap();
        let mut map = py_layer.map.lock().unwrap();
        let source = SoundSource::navigate_to_object(&py_layer, &mut map)?;
        Ok(Some(match attr {
            "position" => point_to_tup(source.area.position()).into_py(py),
            "looping" => source.looping.into_py(py),
            "panning" => source.panning.into_py(py),
            "delay" => source.delay.into_py(py),
            "falloff" => source.falloff.into_py(py),
            "position_env" => source.position_env.into_py(py),
            "position_env_offset" => source.position_env_offset.into_py(py),
            "sound_env" => source.sound_env.into_py(py),
            "sound_env_offset" => source.sound_env_offset.into_py(py),
            "shape" => match &source.area {
                SoundArea::Rectangle(rect) => point_to_tup(rect.extent()).into_py(py),
                SoundArea::Circle(disk) => fixed_py(disk.radius).into_py(py),
            },
            _ => return Ok(None),
        }))
    }

    fn setattr(
        element: &PySource,
        attr: &str,
        value: PyObject,
        py: Python,
    ) -> PyResult<Option<()>> {
        let py_layer = element.depythonize().lock().unwrap();
        let mut map = py_layer.map.lock().unwrap();
        let source = SoundSource::navigate_to_object(&py_layer, &mut map)?;
        match attr {
            "position" => source.area.set_position(depy2(value, py)?),
            "looping" => source.looping = value.extract(py)?,
            "panning" => source.panning = value.extract(py)?,
            "delay" => source.delay = value.extract(py)?,
            "falloff" => source.falloff = value.extract(py)?,
            "position_env" => source.position_env = value.extract(py)?,
            "position_env_offset" => source.position_env_offset = value.extract(py)?,
            "sound_env" => source.sound_env = value.extract(py)?,
            "sound_env_offset" => source.sound_env_offset = value.extract(py)?,
            "shape" => {
                let pos = source.area.position();

                if let Ok((w, h)) = value.extract(py) {
                    source.area =
                        SoundArea::Rectangle(Rect::new(pos.x, pos.y, py_fixed(w)?, py_fixed(h)?));
                } else if let Ok(radius) = value.extract(py) {
                    source.area = SoundArea::Circle(Disk::new(pos, py_fixed(radius)?));
                } else {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        "Shape must be either radius or (width, height)",
                    ));
                }
            }
            _ => return Ok(None),
        }
        Ok(Some(()))
    }
}

do_entry_impls!(SoundSource, PySource, PySources,);

do_sequence_impls!(
    SoundSource,
    PySource,
    PySources,
    /// Constructs a new sound source with the editor defaults.
    #[pyo3(name = "new")]
    fn py_new(&self) -> PyResult<PySource> {
        let new_source = SoundSource::default();
        append_wrapped(self, new_source)
    }
);
