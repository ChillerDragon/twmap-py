#!/usr/bin/env python3

from typing import Dict
from typing_extensions import Optional

import re

py2rs_mappings = {
    'Image': 'src/images.rs',
    'Images': 'src/images.rs',
    'Group': 'src/groups.rs',
    'Groups': 'src/groups.rs',
    'Layer': 'src/layers.rs',
    'Layers': 'src/layers.rs',
    'Quad': 'src/quads.rs',
    'Quads': 'src/quads.rs',
    'Envelope': 'src/envelopes.rs',
    'Envelopes': 'src/envelopes.rs',
    'EnvPoint': 'src/envelope_points.rs',
    'EnvPoints': 'src/envelope_points.rs',
    'Info': 'src/info.rs',
    'Map': 'src/lib.rs',
}

def get_rs_methods(rs_file: str) -> Dict[str, list[str]]:
    methods = {
        'entry': [],
        'sequence': []
    }
    with open(rs_file) as f:
        lines = f.read().splitlines()
        scope: Optional[str] = None
        for line in lines:
            if line == 'do_entry_impls!(' or line == '#[pymethods]':
                scope = 'entry'
            if line == 'do_sequence_impls!(':
                scope = 'sequence'
            if line == ');' or line == '}':
                scope = None
            if not scope:
                continue

            m = re.match(r'^    fn (?P<method>[a-zA-Z_][a-zA-Z_0-9]+)', line)
            if m:
                method = m.group('method')
                methods[scope].append(method)
    return methods

def parse_types(lines: list[str]) -> None:
    classes: Dict[str, list[str]] = {}
    current_class: Optional[str] = None
    errors = 0
    for line in lines:
        m = re.match(r'^class (?P<class>.*):', line)
        if m:
            current_class = m.group('class')
            if not current_class:
                print('error')
                exit(1)
            if current_class not in classes:
                classes[current_class] = []
        m = re.match(r'^    def (?P<method>[a-zA-Z_][a-zA-Z_0-9]+)', line)
        if m:
            if not current_class:
                print("unexpected method outside of a class")
                exit(1)
            method = m.group('method')
            classes[current_class].append(method)

    for class_name, py_methods in classes.items():
        rs_file = py2rs_mappings[class_name]
        rs_methods = get_rs_methods(rs_file)
        if class_name.endswith('s'):
            rs_methods = rs_methods['sequence']
        else:
            rs_methods = rs_methods['entry']

        for py_method in py_methods:
            if py_method in ('__init__', '__len__', '__iter__', '__next__', '__getitem__', 'new'):
                continue
            if py_method not in rs_methods:
                print(f"python type hints has method '{py_method}' which is not found in {rs_file}")
                errors += 1

        for rs_method in rs_methods:
            if rs_method in ('new', 'py_new', '__getattr__', '__setattr__'):
                continue
            if rs_method not in py_methods:
                print(f"the rust file '{rs_file}' defined '{rs_method}' which is not found the type hints twmap.pyi")
                errors += 1

    if errors != 0:
        print(f"failed with {errors} errors")
        exit(1)
    print('OK')


with open('twmap.pyi') as f:
    content = f.read().splitlines()
    parse_types(content)

